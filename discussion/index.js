/*
	SELECTION CONTROL STRUCTURE:
		sorts out whether the statement or statement are to be executed based on the condition whether it is true or false

		if else, switch, try catch finally statement
	

		if else statement:
			Syntax:
				if(condition){
					statement
				} else{
					statement
				}

	if statement:
		executs a statement if a specified condition is true
		can stand alone even without else staement

			Syntax:
				if(condition){
					statement
				}

*/
let numA = -1;
if(numA < 0){
	console.log("Hello");
}
console.log(numA < 0);




function findHeight(height){
	if(height < 150){
	console.log("Did not passed min height req.");
		}
	else{
		console.log("Passed the minimum height req.")
}
}
findHeight(149);





function determineTyphoonIntensity(windSpeed){
	if(windSpeed < 30){
		return "not a typhoon yet."
	}else if (windSpeed <= 61){
		return "tropical depression detected."
	}else if (windSpeed >= 62 && windSpeed <= 88){
		return "tropical storm detected"
	}else if (windSpeed >= 89 && windSpeed <= 117){
		return "severe tropical storm detected"
	}else{
		return "typhoon detected"
	}
}
message = determineTyphoonIntensity(70);
console.log(message);

if(message === "tropical storm detected"){
	console.warn(message);
	console.error(message)
}


/*
	truthy and falsy

	-truthy value is a value that is considered TRUE when encountered in a boolean context
	-values are considered true unless defined otherwise: 
	-falsy values or exception for truthy
	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN
*/

// truthy example:

if(true){
	console.log("Truthy");
}
if(1){
	console.log("Truthy");
}
if([]){
	console.log("Truthy");
}


// falsy examples

if(false){
	console.log("falsy");
}
if(0){
	console.log("falsy");
}
if(undefined){
	console.log("falsy");
}


/*

	conditional or ternary operator

	Syntx:
		(expression) ? ifTrue : ifFalse;

*/

let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary operator: "+ternaryResult);


// multiple statement execution

let name = prompt("Enter your name: ");
function isOfLegalAge(){
	//name = "John";
	return "You are of the legal age.";
}
function isUnderAge(){
	//name="Jane";
	return "You are under the age limit."
}
let age1 = parseInt(prompt("What is your age?"));
console.log(age1);
console.log(typeof age1);
let legalAge = (age1 > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: "+legalAge + ", "+name);



/*
	switch statement:

	Syntax:
		switch(expression){
			case value:
				statement;
				break;
			default:
				statement;
				break;
		}
*/

let day = prompt("what day of the week is it today?").toLowerCase()
console.log(day);
switch(day){
	case "monday":
		console.log("the color of the day is red.");
		break;
	case "tuesday":
		console.log("the color of the day is orange.");
		break;
	case "wednesday":
		console.log("the color of the day is yellow.");
		break;
	case "thursday":
		console.log("the color of the day is green.");
		break;
	case "friday":
		console.log("the color of the day is blue.");
		break;
	case "saturday":
		console.log("the color of the day is indigo.");
		break;
	case "sunday":
		console.log("the color of the day is violet.");
		break;
	default:
		console.log("Please input a valid day.");
		break;
}


/*
	Try Catch Finally Statement
	commonly used for error handling
*/

function showIntensityAlert(windSpeed){
	try{
		// attempt to execute a code.
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch(error){
		// catch errors with TRY statement
		console.log(error);
		console.log(error.message);
	}
	finally{
		// it will continue the execution of code regardless of success and failure of code execution in the TRY block to handle or resolve errors
		alert("Intensity updates will show new alert.");
	}
}
showIntensityAlert(56);





////////////////////////////////



